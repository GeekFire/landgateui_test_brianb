import { Injectable } from '@angular/core';
import { delay, map} from 'rxjs/operators';
import { Observable, of} from 'rxjs';
import {PersonModel} from '../models/person-model';
import { PeopleModule } from '../components/people/people.module';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor() { }

  private mockPeopleList = [
    {firstName: 'John', lastName: 'Doe', age: '21', workTitle: 'Wanna be Signer'},
    {firstName: 'Jane', lastName: 'Doe', age: '22', workTitle: 'Signer'},
    {firstName: 'Bob', lastName: 'Barker', age: '80', workTitle: 'TV Host'},
    {firstName: 'John', lastName: 'Doe', age: '21', workTitle: 'Wanna be Signer'},
  ];


  getPeople(): Observable<PersonModel[]> {
    // TODO: Finish this implementation using the data from mockPeopleList
    // of(true).pipe(delay(100))

    let peopleList: PersonModel[] = [];

    this.mockPeopleList.map(person => {
        let newPerson = new PersonModel(person);
        peopleList.push(newPerson);
   })
    return of(peopleList).pipe(delay(100));
  }
}
