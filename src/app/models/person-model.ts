export class PersonModel {

  public firstName: string;
  public lastName: string;
  public age: number;
  public jobTitle: string;

  constructor(dataIn: any) {
    // TODO: Implement a dataIn object that gets passed in as a JavaScript Object


    try{
      for(let index in dataIn){

        switch ( index){
          case "firstName":
            this.firstName = dataIn[index].trim();
            break;
          case "lastName":
            this.lastName = dataIn[index].trim();
            break;
          case "age":
            this.age = +dataIn[index].trim();
            break;
          default:
            this.jobTitle = dataIn[index].trim();
        }
      }
    }catch (err) {
      throw err;
    }
  }
}
