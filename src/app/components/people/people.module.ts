import {NgModule} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { PeopleNgrxModule } from '../people-ngrx/people-ngrx.module';
import {PeopleComponent} from './people.component';

@NgModule({
  declarations: [PeopleComponent],
  exports: [PeopleComponent],
  imports: [BrowserModule, ReactiveFormsModule, PeopleNgrxModule]
})
export class PeopleModule {}

