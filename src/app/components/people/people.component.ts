import { Component, OnInit } from '@angular/core';
import { PersonModel } from 'src/app/models/person-model';
import { PeopleService } from 'src/app/services/people.service';



@Component({
  selector: 'app-people-component',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  // TODO: Include the PersonService and get the list of People

  peopleList: PersonModel[];
  currentPerson: number;

  constructor(
    private people: PeopleService,
  ) {
    this.people.getPeople().subscribe((people) => this.peopleList = people)
  }

  ngOnInit() {

  }

  switchPerson(i: number){
    this.currentPerson == i ? this.currentPerson = null : this.currentPerson = i;
  }

  finshedEditing($event, i){

    if ($event != true){
      this.peopleList[i].firstName = $event.firstName;
      this.peopleList[i].lastName = $event.lastName;
      this.peopleList[i].age = $event.age;
      this.peopleList[i].jobTitle = $event.jobTitle;
    }
    this.currentPerson = null;
  }
}
