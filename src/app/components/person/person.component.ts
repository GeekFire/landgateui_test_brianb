import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { PersonModel } from 'src/app/models/person-model';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  @Input() person: PersonModel;
  @Output() finished = new EventEmitter();
  userDetails: FormGroup;


  constructor(
    private fb: FormBuilder
  ) {
    this.userDetails = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      age: ['', Validators.required],
      jobTitle: ['', Validators.required]
    });


  }

  ngOnInit(): void {
    this.userDetails.controls['firstName'].setValue(this.person.firstName);
    this.userDetails.controls['lastName'].setValue(this.person.lastName);
    this.userDetails.controls['age'].setValue(this.person.age);
    this.userDetails.controls['jobTitle'].setValue(this.person.jobTitle);
  }

  onSubmit(){

    this.finished.emit({
      firstName: this.userDetails.controls['firstName'].value,
      lastName: this.userDetails.controls['lastName'].value,
      age: this.userDetails.controls['age'].value,
      jobTitle: this.userDetails.controls['jobTitle'].value
    });
  }

  done(){
    this.finished.emit(true);
  }
}
