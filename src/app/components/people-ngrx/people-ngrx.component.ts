import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PersonModel } from 'src/app/models/person-model';
import { PeopleService } from 'src/app/services/people.service';
import { updatePerson, loadPeoples} from './ngrx/actions/people.actions';
import { selectPeopleState } from './ngrx/selectors/people.selectors';



@Component({
  selector: 'app-people-component',
  templateUrl: './people-ngrx.component.html',
  styleUrls: ['./people-ngrx.component.scss']
})
export class PeopleNgrxComponent implements OnInit {

  people$: any;
  currentPerson: number;
  // TODO: Include the Store and get the data from the NgrxStore

  constructor(
    private peopleService: PeopleService,
    private store: Store,
  ) {
    this.store.select(selectPeopleState).subscribe(data => this.people$ = data.people);
  }

  ngOnInit() {
    this.peopleService
      .getPeople()
      .subscribe((people) => {
        this.store.dispatch(loadPeoples({ people }));
      })
  }

  onUpdate(person: PersonModel, index: number){

    this.store.dispatch(updatePerson({person, index}));
  }

  finshedEditing($event, i){

    if($event != true){
      this.onUpdate($event, i);

    }
    this.closeEditor()
  }

  switchPerson(i: number){
    this.currentPerson == i ? this.currentPerson = null : this.currentPerson = i;
  }

  closeEditor(){
    this.currentPerson = null;
  }



}
