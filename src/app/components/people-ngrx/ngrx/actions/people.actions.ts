import { createAction, props } from '@ngrx/store';
import {PersonModel} from '../../../../models/person-model';

// This is a request
export const loadPeoples = createAction(
  '[People] Load People',
  props<{people: Array<PersonModel> }>(),
);

export const updatePerson = createAction(
  '[People] Update Person',
  props<{person: PersonModel, index: number}>(),
)

// This is a load success
export const loadedPeople = createAction(
  '[People] Loaded',
  (people: PersonModel[]) => ({people})
)




