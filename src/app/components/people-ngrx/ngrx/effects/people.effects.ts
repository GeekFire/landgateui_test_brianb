import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { concatMap, mergeMap, map, catchError } from 'rxjs/operators';
import {EMPTY, of} from 'rxjs';

import * as PeopleActions from '../actions/people.actions';
import {loadedPeople} from '../actions/people.actions';
import { PeopleService } from 'src/app/services/people.service';



@Injectable()
export class PeopleEffects {


  loadPeoples$ = createEffect(() => {
    return this.actions$.pipe(
      ofType('[People] Load People'),

      // TODO Implement a Load People Action that gets the data from the service.
      mergeMap(()=> this.peopleService.getPeople()
      .pipe(
        map(people => ({ type: '[People] Loaded', payload: people})),
        catchError(() => EMPTY)
      )),

      concatMap(() => of(loadedPeople([])))
    )
  });

  constructor(
    private actions$: Actions,
    private peopleService: PeopleService,
    ) {}

}
