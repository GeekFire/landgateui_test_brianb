

import { state } from '@angular/animations';
import { Action, createReducer, on } from '@ngrx/store';

import { PersonModel } from 'src/app/models/person-model';
import * as PeopleActions from '../actions/people.actions';


export const peopleFeatureKey = 'people';

// TODO: Need to add People to the store and initialize it.
export interface State {
  people: PersonModel[];
}

export const initialState: State = {
  people: []
};

export const reducer = createReducer(
  initialState,

  on(PeopleActions.loadPeoples, (state, {people}) => ({
     people
  })),

  on(PeopleActions.updatePerson, (state ,action) => {

    return {
      ...state,
      people: [
        ...state.people.slice(0, action.index),
        {
          ...state.people[action.index],

          firstName:  action.person.firstName,
          lastName:   action.person.lastName,
          age:        action.person.age as number,
          jobTitle:   action.person.jobTitle
        },
        ...state.people.slice(action.index + 1)
      ]
    }
  })
);

