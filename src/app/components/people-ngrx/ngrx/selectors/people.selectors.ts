import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PersonModel } from 'src/app/models/person-model';
import * as fromPeople from '../reducers/people.reducer';


export const selectPeopleState = createFeatureSelector<fromPeople.State>(
  fromPeople.peopleFeatureKey
);

// TODO: need to add a selector for people.


export const selectPeople = createSelector (
  selectPeopleState,
  (state) => {
    return state.people.map((person) => state.people.find((personInPeople) => person === personInPeople))
  }

)
