import { NgModule } from '@angular/core';
import { PeopleNgrxComponent } from './people-ngrx.component';
import { StoreModule } from '@ngrx/store';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import * as fromPeople from './ngrx/reducers/people.reducer';
import { PeopleEffects } from './ngrx/effects/people.effects';
import { AppModule } from 'src/app/app.module';
import { PersonComponent } from '../person/person.component';
import { PeopleModule } from '../people/people.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [PeopleNgrxComponent, PersonComponent],
  exports: [PeopleNgrxComponent, PersonComponent],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    StoreModule.forFeature(fromPeople.peopleFeatureKey, fromPeople.reducer),
    EffectsModule.forFeature([PeopleEffects]),],

})
export class PeopleNgrxModule {
}
